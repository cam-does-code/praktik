require "application_system_test_case"

class UserTagsTest < ApplicationSystemTestCase
  setup do
    @user_tag = user_tags(:one)
  end

  test "visiting the index" do
    visit user_tags_url
    assert_selector "h1", text: "User tags"
  end

  test "should create user tag" do
    visit user_tags_url
    click_on "New user tag"

    fill_in "Tag", with: @user_tag.tag_id
    fill_in "User info", with: @user_tag.user_info_id
    click_on "Create User tag"

    assert_text "User tag was successfully created"
    click_on "Back"
  end

  test "should update User tag" do
    visit user_tag_url(@user_tag)
    click_on "Edit this user tag", match: :first

    fill_in "Tag", with: @user_tag.tag_id
    fill_in "User info", with: @user_tag.user_info_id
    click_on "Update User tag"

    assert_text "User tag was successfully updated"
    click_on "Back"
  end

  test "should destroy User tag" do
    visit user_tag_url(@user_tag)
    click_on "Destroy this user tag", match: :first

    assert_text "User tag was successfully destroyed"
  end
end
