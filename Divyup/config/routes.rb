Rails.application.routes.draw do
  resources :user_tags
  resources :user_infos
  resources :tags
  resources :shifts
  resources :calendars
  resources :calendar_groups
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  get 'emails/new'
  get 'emails/create'
  get 'emails/edit'
  get 'emails/update'
  resources :users
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  # root "posts#index"
end
