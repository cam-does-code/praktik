class PolicyAccessDenied < StandardError
    def initialize(msg="Access Denied")
        super
    end
end