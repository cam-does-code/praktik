class UserTagsController < ApplicationController
  before_action :set_user_tag, only: %i[ show edit update destroy ]

  # GET /user_tags or /user_tags.json
  def index
    @user_tags = UserTag.all
  end

  # GET /user_tags/1 or /user_tags/1.json
  def show
  end

  # GET /user_tags/new
  def new
    @user_tag = UserTag.new
  end

  # GET /user_tags/1/edit
  def edit
  end

  # POST /user_tags or /user_tags.json
  def create
    @user_tag = UserTag.new(user_tag_params)

    respond_to do |format|
      if @user_tag.save
        format.html { redirect_to user_tag_url(@user_tag), notice: "User tag was successfully created." }
        format.json { render :show, status: :created, location: @user_tag }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_tags/1 or /user_tags/1.json
  def update
    respond_to do |format|
      if @user_tag.update(user_tag_params)
        format.html { redirect_to user_tag_url(@user_tag), notice: "User tag was successfully updated." }
        format.json { render :show, status: :ok, location: @user_tag }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_tags/1 or /user_tags/1.json
  def destroy
    @user_tag.destroy!

    respond_to do |format|
      format.html { redirect_to user_tags_url, notice: "User tag was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_tag
      @user_tag = UserTag.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_tag_params
      params.require(:user_tag).permit(:user_info_id, :tag_id)
    end
end
