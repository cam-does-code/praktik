class SessionsController < ApplicationController
	# skip_authorization_check
	def new
	end
  #todo: add auth_token to usermodel >.>
	def create
	  user = User.where(email: params[:email]).first
		if user && user.authenticate(params[:password])
		  if params[:remember_me]
			cookies.permanent[:auth_token] = user.auth_token
		  else
			cookies[:auth_token] = user.auth_token
		  end
		  redirect_to root_url, notice: "Logget ind!"
		else
		  flash.now.alert = "Ugyldig email eller password"
		  render "new"
		end
	end
  
	def destroy
	  current_user.invalidate_sessions! 
	  cookies.delete(:auth_token)
	  redirect_to log_in_path, notice: "Logget ud"
	end
  end