class EmailsController < ApplicationController
  def new
  end

  def create
	user = User.where(email: params[:email.first])
	user.send_password_reset(request.host_with_port) if user
	redirect_to login_path, notice: "Oprettelses email sendt til ny bruger"
  end

  def edit
	@user = User.find_by(password_reset_token: params[:id])
	if @user.nil?
		redirect_to new_password_reset_path, notice: "Linket er udløbet eller ugyldigt"
	elsif @user.password_reset_sent_at < 2.hours.ago
		redirect_to new_password_reset_path, notice: "Linket er udløbet eller ugyldigt"
	end
  end

  def update
    @user = User.find_by(password_reset_token: params[:password_reset_token])
    if @user.update!(password_reset_params)
      @user.update!(:password_reset_token, nil)
      if @user.invitation_complete
        notice = "Kodeord ændret for #{user.email}"
      else
        @user.update!(:invitation_complete, true)
        notice = "Profil Oprettet for #{user.email}"
      end
      redirect_to login_path, notice: notice
    else
      render :edit
    end
  end

  #sends the invite
  def send_invite
    user = user = User.find_or_initialize_by(email: params[:user][:email])
    unless user.invitation_complete
      user.send_invitation(current_user.email)
      redirect_to users_admin_path, notice: "Invitation sendt til #{user.email}."
    else
      redirect_to users_admin_path, notice: "En bruger for #{user.email} eksisterer allerede."
    end
  end

  #load the page
  def invite
	@user = User.new
  end

  #lets them confirm, invite link confirmation
  def confirm
    @user = User.find_by(password_reset_token: params[:password_reset_token])
    if @user.nil?
      redirect_to root_url, notice: "Linket er ugyldigt, kontakt venligst din organisations administrator"
    end
  end
  
 
  # Only allow a list of trusted parameters through.
  def password_reset_params
	params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
