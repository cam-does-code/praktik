class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	helper_method :current_user
	helper_method :authorized?
	rescue_from PolicyAccessDenied do |exception|
		if current_user
		  redirect_to root_url, notice: exception 
		else
		  redirect_to login_path
		end
	end

	def authorized?(action="#{action_name}", controller="#{self.class.to_s.gsub('::', '_').gsub('Controller', '').underscore.downcase}")
		raise PolicyAccessDenied unless current_user
		current_user.authorize(current_user.role, controller, action)
	  end
	
	  def authorize 
		redirect_to root_url unless authorized?
	  end
	
	  private
	  
	  def current_user 
		@current_user ||= User.find_by(auth_token: cookies[:auth_token]) if cookies[:auth_token]
	  end
end
