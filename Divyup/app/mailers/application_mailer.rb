class ApplicationMailer < ActionMailer::Base
  default from: "from@example.com", :charset => "UTF-8"
  layout "mailer"
end
