class Calendar < ApplicationRecord
  belongs_to :calendar_group
  has_many :shifts
end
