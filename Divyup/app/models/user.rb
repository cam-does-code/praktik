class User < ApplicationRecord
	has_secure_password
	has_many :tags, through: :user_tags
	has_many :user_tags
	has_one :user_info, dependent: :destroy

	before_create {generate_token(:auth_token)}

	before_validation :downcase_email
	before_validation :set_values

	validates :email,
		presence: true,
		length: {minimum: 3, maximum: 254, message: "Email too short"},
		uniqueness: true,
		format: {with: /@/, message: "address invalid"}

	validates_presence_of :password, on: :create

end

def invalidate_sessions! 
	generate_token(:auth_token)
	save!
end

def send_invitation(host)
	generate_token(:password_reset_token)
	pass = SecureRandom.urlsafe_base64
	self.password_reset_sent_at = Time.zone.now
	self.password = pass.reverse
	self.password_confirmation
	self.save
	usermailer.invite_new_user(self, host).deliver_now
end

def send_passwordreset(host)
	generate_token(:password_reset_token)
	self.password_reset_sent_at = Time.zone.now
	self.save
	usermailer.password_reset(self, host).deliver_now
end


def has_role?(sym)
	!((self.role & [sym.to_s]).empty?)
end

def view_role?(sym)
	return self.has_role?(sym) ? "\u2713" : ""
end

def toggle_role(sym)
	if self.has_role? sym 
		self.add_role sym 
	else
		self.del_role sym
	end
end

def add_role(sym)
	self.role.push(sym.to_s) unless self.role.include?(sym.to_s)
	save!
end

def del_role(sym)
	self.role.delete(sym.to_s)
	save!
end

def roles
	{
		admin: "Admin"
	}
end

private

def set_values
	self.role.push('user') unless self.role.include?('user')
end


def downcase_email
	self.email = self.email.downcase if self.email.present?
end

def generate_token(column)
	begin
		self[column] = SecureRandom.urlsafe_base64
	end while User.exists?(column => self[column])
end