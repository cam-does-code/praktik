class AddRolesToUser < ActiveRecord::Migration[7.1]
	def change
	# Postgres
	  # add_column :users, :role, :string, array: true, default: []
	# SQLite
	  add_column :users, :role, :json, default: [], null: false
	  add_check_constraint :users, "JSON_TYPE(role) = 'array'", name: 'user_role_is_array'
	end
  end