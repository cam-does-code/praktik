class RemoveNameFromUser < ActiveRecord::Migration[7.1]
  def change
    remove_column :users, :user, :string
  end
end
