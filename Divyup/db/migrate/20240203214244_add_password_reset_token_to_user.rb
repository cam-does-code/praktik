class AddPasswordResetTokenToUser < ActiveRecord::Migration[7.1]
  def change
    add_column :users, :password_reset_token, :string
    add_column :users, :password_reset_sent_at, :datetime
    add_column :users, :invitation_complete, :boolean
  end
end
