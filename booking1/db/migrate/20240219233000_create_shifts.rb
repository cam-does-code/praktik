class CreateShifts < ActiveRecord::Migration[7.1]
  def change
    create_table :shifts do |t|
      t.string :name
      t.text :description
      t.references :calendar, null: false, foreign_key: true
      t.datetime :start_time
      t.decimal :duration
      t.decimal :break_duration

      t.timestamps
    end
  end
end
