class CreateCalendars < ActiveRecord::Migration[7.1]
  def change
    create_table :calendars do |t|
      t.string :name
      t.text :description
      t.references :calendar_group, null: false, foreign_key: true

      t.timestamps
    end
  end
end
