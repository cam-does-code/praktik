# This file should ensure the existence of records required to run the application in every environment (production,
# development, test). The code here should be idempotent so that it can be executed at any point in every environment.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Example:
#
#   ["Action", "Comedy", "Drama", "Horror"].each do |genre_name|
#     MovieGenre.find_or_create_by!(name: genre_name)
#   end

user = User.find_or_initialize_by(email:"admin@example.com") 
user.password = "password"
user.password_confirmation = "password"
user.admin!
user.save!

user = User.find_or_initialize_by(email:"user@example.com") 
user.password = "password"
user.password_confirmation = "password"
user.save!

["Group1", "Group2"].each do |group_name|
    group = CalendarGroup.find_or_create_by!(name: group_name)
    group.id.times do |cal_name|
        cal = Calendar.find_or_create_by!(name: "Cal #{group.id}x#{cal_name}", calendar_group: group)
        cal.id.times do |shft| 
            new_shift = Shift.find_or_initialize_by(name: "Shift #{cal.id}x#{shft}")
            new_shift.calendar = cal
            new_shift.start_time = shft.hours.from_now 
            new_shift.duration = shft
            new_shift.break_duration = 1
            new_shift.save!
        end
    end
end

