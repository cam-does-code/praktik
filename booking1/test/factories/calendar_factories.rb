FactoryBot.define do
    factory :calendar_group do
        sequence(:name) { |n| "Calendar Group #{n}" }
        description { "This is a calendar group" }
    end

    factory :calendar do
        sequence(:name) { |n| "Calendar #{n}" }
        description { "This is a calendar" }
        calendar_group
    end

    factory :shift do 
        sequence(:name) { |n| "Shift #{n}"}
        description { "This is a shift" }
        start_time { 2.days.from_now }
        duration { 8.0 }
        break_duration { 1.0 } 
        calendar
    end
end