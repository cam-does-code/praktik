FactoryBot.define do
    factory :user do 
        sequence(:email) { "user#{_1}@example.com" }
        password { "password" }
        password_confirmation { "password" }
        confirmed_at { Date.today }
    end
    factory :user_calendar do
        user 
        calendar
    end
end