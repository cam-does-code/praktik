require "test_helper"

class CalendarGroupTest < ActiveSupport::TestCase
  test "CalGroup should not save without name" do
    cg = CalendarGroup.new
    assert_not cg.save, "Saved without a name"
  end


end
