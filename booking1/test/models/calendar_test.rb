require "test_helper"

class CalendarTest < ActiveSupport::TestCase

  test "Calendar doesn't save without a name" do
    cal = Calendar.new
    assert_not cal.save, "Saved with no name"
  end

end
