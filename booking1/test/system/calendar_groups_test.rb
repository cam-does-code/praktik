require "application_system_test_case"
=begin
class CalendarGroupsTest < ApplicationSystemTestCase
  setup do
    @calendar_group = calendar_groups(:one)
  end

  test "visiting the index" do
    visit calendar_groups_url
    assert_selector "h1", text: "Calendar groups"
  end

  test "should create calendar group" do
    visit calendar_groups_url
    click_on "New calendar group"

    fill_in "Description", with: @calendar_group.description
    fill_in "Name", with: @calendar_group.name
    click_on "Create Calendar group"

    assert_text "Calendar group was successfully created"
    click_on "Back"
  end

  test "should update Calendar group" do
    visit calendar_group_url(@calendar_group)
    click_on "Edit this calendar group", match: :first

    fill_in "Description", with: @calendar_group.description
    fill_in "Name", with: @calendar_group.name
    click_on "Update Calendar group"

    assert_text "Calendar group was successfully updated"
    click_on "Back"
  end

  test "should destroy Calendar group" do
    visit calendar_group_url(@calendar_group)
    click_on "Destroy this calendar group", match: :first

    assert_text "Calendar group was successfully destroyed"
  end
end
=end