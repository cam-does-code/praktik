import { Controller } from "@hotwired/stimulus";
import { Modal } from "flowbite";

// Connects to data-controller="modal"
export default class extends Controller {
  connect() {
    this.modal = new Modal(document.getElementById("modal"));
    console.log("modal connect");
  }
  open() {
    if (!this.modal.isVisible()) {
      this.modal.show();
      console.log("modal open");
    }
  }
  close(e) {
    if (e.detail.success) {
      this.modal.hide();
      console.log("modal close");
    }
  }
  closeButton(e){
    this.modal.hide();
    console.log("modal closeButton")
  }
}
