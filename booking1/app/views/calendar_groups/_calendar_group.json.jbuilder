json.extract! calendar_group, :id, :name, :description, :created_at, :updated_at
json.url calendar_group_url(calendar_group, format: :json)
