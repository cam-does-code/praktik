module ApplicationHelper
    def flowicon(icon)
        case icon
        when "plus-circle"
            content_tag(:svg, class: %(w-[24px] h-[24px]), "aria-hidden" => "true", xmlns: "http://www.w3.org/2000/svg", fill: "none", viewBox: "0 0 24 24") do 
                tag(:path, stroke: "currentColor", "stroke-linecap" => "round", "stroke-linejoin" => "round", "stroke-width" => "2", d: "M12 7.8v8.4M7.8 12h8.4m4.8 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z")
            end
        end

    end
end
