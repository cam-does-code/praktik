class CalendarGroup < ApplicationRecord
  has_many :calendars

  validates :name, { presence: true }

  scope :ordered, -> { order(name: :asc) }
end
