class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :registerable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, 
         :recoverable, :rememberable, :validatable
  enum role: { standard: 0, admin: 1 }

  has_many :user_calendars
  has_many :calendars, through: :user_calendars
  has_many :calendar_groups, through: :calendars

  def groups
    if self.role = :admin
      return CalendarGroup.all
    else
      return self.calendar_groups
    end
  end

  def group_calendars(grp)
    if self.role = :admin
      return grp.calendars
    else
      return self.calendars.where(calendar_group: grp)
    end
  end

end
