class Calendar < ApplicationRecord
  belongs_to :calendar_group
  has_many :shifts

  has_many :user_calendars
  has_many :users, through: :user_calendars

  validates :name, { presence: true }
  validates :calendar_group, { presence: true }
  
end
