require "test_helper"

class CalendarGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @calendar_group = calendar_groups(:one)
  end

  test "should get index" do
    get calendar_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_calendar_group_url
    assert_response :success
  end

  test "should create calendar_group" do
    assert_difference("CalendarGroup.count") do
      post calendar_groups_url, params: { calendar_group: { description: @calendar_group.description, name: @calendar_group.name } }
    end

    assert_redirected_to calendar_group_url(CalendarGroup.last)
  end

  test "should show calendar_group" do
    get calendar_group_url(@calendar_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_calendar_group_url(@calendar_group)
    assert_response :success
  end

  test "should update calendar_group" do
    patch calendar_group_url(@calendar_group), params: { calendar_group: { description: @calendar_group.description, name: @calendar_group.name } }
    assert_redirected_to calendar_group_url(@calendar_group)
  end

  test "should destroy calendar_group" do
    assert_difference("CalendarGroup.count", -1) do
      delete calendar_group_url(@calendar_group)
    end

    assert_redirected_to calendar_groups_url
  end
end
