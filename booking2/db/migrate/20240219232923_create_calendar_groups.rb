class CreateCalendarGroups < ActiveRecord::Migration[7.1]
  def change
    create_table :calendar_groups do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
