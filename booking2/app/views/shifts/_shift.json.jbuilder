json.extract! shift, :id, :name, :description, :calendar_id, :start_time, :duration, :break, :created_at, :updated_at
json.url shift_url(shift, format: :json)
