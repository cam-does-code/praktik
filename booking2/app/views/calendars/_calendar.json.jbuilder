json.extract! calendar, :id, :name, :description, :calendar_group_id, :created_at, :updated_at
json.url calendar_url(calendar, format: :json)
