# Praktik



## Logbog
Gennem hele forløbet arbejdes ift. Scrum principper, med daglige standup møder og sparring med en projektleder    

### Uge 1

**22/1**

Dagens Agenda:

- Introduktion til firmaet og deres systemer

**23/1**

Dagens Agenda:

- lære deres nuværende system at kende
- Snakke om hvad der skal laves
- Undersøge løsninger

**24/1**

Agenda:
- Kigge på open source booking systemer

**25/1**

- Snakket om deres hjemmeside, de gerne vil have lidt hjælp til at lave om
- Kigge på open source booking systemer

**26/1**
- finde ud af hvordan deres hjemmeside er lavet
- Kigge på database

### Uge 2

Ugens Agenda:
- Få noget til at køre på Pi
- Kode, database
- Remote access til Pi (ssh?)

**2/2**
- Kørende på: http://72.42.157.173:3000/users

Noter: Efter problemer med Pi, kører vi nu på en midlertidig server mens der bliver udviklet - vil skulle flyttes hvis det skal tages i brug når færdigt

### Uge 3

Agenda:
- kode, og mere kode.
- lidt arbejde på at opdatere hjemmeside

**6/2**
Lang dag i Haderslev:
- vise nuværende kode
- skrive mere kode
- Lære lidt mere om firmaet, og møde deres terapiheste :D
- snakket om mangler ved deres nuværende system

**8/2**
- kode
- linkedin learning
- org chart    
![Org Chart](Resources/orgchart.drawio.png)

**9/2**
- Model diagrammer, UML?: https://drive.google.com/file/d/1gnU9axYZRnJSF2TQbEi_bLqEv7HrRmFR/view?usp=sharing
- LinkedIn Learning Ruby on Rails kursus
- Mere kode

     
Noter: sqlite - evt. fordele ved postgres? sqlite bedre ift. app udvikling senere hen?

### Uge 4
https://www.linkedin.com/learning/certificates/9917024d60ca313f2f738f4191b7ace2bb1a6e862e35bc6d78907050b6979cab    

**12/2**
- kode, og videre på det fra sidste uge.

**16/2**
- Hjemmeside, kode    

Noter: forsøgt på RPi igen, brændt den af........    

### Uge 5

Finde ud af det visuelle..      
Ressourcer:      
https://flowbite.com/docs/components/sidebar/    
https://www.youtube.com/watch?v=QJ2cGu82I8k    
https://www.youtube.com/watch?v=k0PeogzqwlM    
https://flowbite.com/docs/getting-started/rails/    
https://flowbite.com/docs/components/accordion/    
https://heroicons.com/     
     
formularer:    
https://github.com/heartcombo/simple_form    
    
tailwind css:    
https://github.com/abevoelker/simple_form_tailwind_css

authentication system:     
https://github.com/heartcombo/devise    
    
i18n plugin:    
https://github.com/tigrish/devise-i18n    
    
invitation system:    
https://github.com/scambra/devise_invitable

authorization system (Måske, ikke sat op endnu):    
https://github.com/varvet/pundit    
tutorials:     
https://blog.dennisokeeffe.com/blog/2022-03-09-part-8-policy-authorization-in-rails-7-with-pundit
og 
https://www.youtube.com/watch?v=SodfIjgcaW8      
        
https://www.hotrails.dev/
      
Til senere:     
https://www.colby.so/posts/turbo-rails-101-todo-list    
https://www.colby.so/posts/turbo-streams-on-rails    
https://www.colby.so/posts/turbo-frames-on-rails    
https://nts.strzibny.name/single-attribute-in-place-editing-turbo/     

    
**20/2**
- Snakke med Per omkring Airthings, UCL    

**21/2**
- Allan møde 11:30

### Uge 6
- Kalendere og events i booking
- visuelt

### Uge 7
- problemer med vagter, kalendere, grupper. Skal kigges på

### Uge 8
- Kan nu lave nye kalendere og redigere nuværende
- Kan slette kalender grupper, kun hvis kalender gruppe ikke har tilknyttede kalendere
- kan ikke tilføje vagter endnu

### Uge 9
- fixe pop up vinduer ift. redigering
- tilføje brugere til database???
- tilføje vagter
- Onsdag fejres min fødselsdag på arbejdet :D

### Uge 10
- Primært fokus på dokumentation og hand-off

### Ressourcer:
https://easyappointments.org/    
https://alf.io/    
https://crozdesk.com/software/mhelpdesk/    
https://dev.to/kevinluo201/building-a-simple-authentication-in-rails-7-from-scratch-2dhb    
